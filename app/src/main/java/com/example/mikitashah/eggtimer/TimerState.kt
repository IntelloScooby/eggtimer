package com.example.mikitashah.eggtimer

enum class TimerState {
    TIMER_STARTED,
    TIMER_STOPPED,
    TIMER_ENDED
}