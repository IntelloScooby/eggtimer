package com.example.mikitashah.eggtimer

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.media.MediaPlayer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.SeekBar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), SeekBar.OnSeekBarChangeListener, View.OnClickListener, LifecycleOwner {

    private val TAG = this.javaClass.simpleName
    private lateinit var timerViewModel: TimerViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        timerViewModel = ViewModelProviders.of(this).get(TimerViewModel::class.java)

        timerViewModel.getTimerValue().observe(this, Observer<String> { timerValue ->
            if (timerValue != null) {
                Log.d(TAG, "timerValue: $timerValue")
                timer_value.text = timerValue
            }
        })

        timerViewModel.getTimerState().observe(this, Observer<TimerState> { timerState ->
            if (timerState != null) {
                when (timerState) {
                    TimerState.TIMER_STARTED -> onTimerStarted()
                    TimerState.TIMER_ENDED -> onTimerEnded()
                    TimerState.TIMER_STOPPED -> onTimerStopped()
                }
            }
        })

        timer_start_stop_button.setOnClickListener(this)
        timer_seek_bar.setOnSeekBarChangeListener(this)
    }

    private fun onTimerStopped() {
        timer_seek_bar.isEnabled = true
        timer_start_stop_button.text = resources.getString(R.string.default_button_text)
    }

    private fun onTimerEnded() {
        playSound()
        timer_seek_bar.isEnabled = true
        timer_start_stop_button.text = resources.getString(R.string.default_button_text)
    }

    private fun onTimerStarted() {
        timer_seek_bar.isEnabled = false
        timer_start_stop_button.text = resources.getString(R.string.stop_button_text)
    }

    private fun playSound() {
        val mediaPlayer = MediaPlayer.create(this, R.raw.air_horn)
        mediaPlayer.start()
    }

    override fun onClick(view: View?) {
        timerViewModel.updateTimer()
    }

    override fun onProgressChanged(seekbar: SeekBar?, progress: Int, fromUser: Boolean) {
        timer_seek_bar.progress = progress
        timerViewModel.setTimerValue(progress)
    }

    override fun onStartTrackingTouch(seekbar: SeekBar?) {
    }

    override fun onStopTrackingTouch(seekbar: SeekBar?) {
    }
}
