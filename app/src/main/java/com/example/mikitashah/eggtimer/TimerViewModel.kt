package com.example.mikitashah.eggtimer

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.os.CountDownTimer
import android.util.Log

/**
 * On clicking the start button, the slider should disappear, the counter should start, the button text should be changed
 * If we stop in the middle of the timerValue, then it resets to 30 seconds, re-enables the slider, change button text
 * When the timerValue finishes, the sound is played, the slider is re-enabled, button text is changed
 */

class TimerViewModel : ViewModel() {

    private val TAG = this.javaClass.simpleName

    private val defaultTime = "00:30"
    private val tickIntervalInMiliSeconds = 1000L
    private val counterBuffer = 100L
    private val defaultCounter = 30 * 1000L

    private var counter: Long = defaultCounter

    private val timerValue = MutableLiveData<String>()
    private val timerState = MutableLiveData<TimerState>()

    private lateinit var countDownTimer: CountDownTimer

    fun getTimerState(): MutableLiveData<TimerState> {
        if (timerState.value == null) {
            timerState.value = TimerState.TIMER_STOPPED
        }
        return timerState
    }

    fun getTimerValue(): MutableLiveData<String> {
        if (timerValue.value == null) {
            timerValue.value = defaultTime
        }
        return timerValue
    }

    fun setTimerValue(valueInSeconds: Int) {
        counter = valueInSeconds * 1000L
        timerValue.value = getTimeInMinsAndSeconds(valueInSeconds)
    }

    private fun startTimer() {
        countDownTimer = object : CountDownTimer(counter + counterBuffer, tickIntervalInMiliSeconds) {

            override fun onTick(timeLeftInMiliSeconds: Long) {
                timerValue.value = getTimeInMinsAndSeconds((timeLeftInMiliSeconds / 1000).toInt())
            }

            override fun onFinish() {
                counter = 0L
                timerValue.value = "00:00"
                timerState.value = TimerState.TIMER_ENDED
            }
        }
        countDownTimer.start()
        timerState.value = TimerState.TIMER_STARTED
    }

    private fun stopTimer() {
        countDownTimer.cancel()
        timerValue.value = defaultTime
        counter = defaultCounter
        timerState.value = TimerState.TIMER_STOPPED
    }

    fun updateTimer() {
        if (timerState.value == TimerState.TIMER_STARTED) {
            stopTimer()
            return
        }
        startTimer()
    }

    private fun getTimeInMinsAndSeconds(timeInSeconds: Int): String {
        val mins = timeInSeconds / 60
        val seconds = timeInSeconds % 60
        var timeInMinsAndSeconds: String
        timeInMinsAndSeconds = if (mins < 10) {
            "0$mins:"
        } else {
            "$mins:"
        }
        timeInMinsAndSeconds = if (seconds < 10) {
            timeInMinsAndSeconds + "0$seconds"
        } else {
            timeInMinsAndSeconds + "$seconds"
        }
        return timeInMinsAndSeconds
    }
}